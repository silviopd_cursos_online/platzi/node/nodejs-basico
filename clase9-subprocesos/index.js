const cp = require('child_process')

const readline = require('readline')
const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout
})

function execCommand(command){
  cp.exec(command, (err,stdout,stderr) => {
    if (err) {
      console.log(`Error: ${err}`)
      return;
      // throw err
    }

    if (stdout) console.log(`Standard out: \n ${stdout}`)

    if (stderr) console.log(`Standard err: \n ${stderr}`)
  })
}

//execCommand("ls")

// execCommand(`node ${__dirname}/src/script.js --base=4`)
// execCommand(`node ${__dirname}/src/script.js --base=3`)
// execCommand(`node ${__dirname}/src/script.js --base=2`)


//RETO CON READLINE
const AskQuestion = (rl, command) => {
  return new Promise( (res,rej) => {
    rl.question(command, ans => {
      res(ans)
    })
  })
}

const Ask = (command) => {
  return new Promise ( async resolve => {
    let results = await AskQuestion(rl,command)

    rl.close()
    resolve(results)
  })
}

Ask('Ingrese base \n').then( n => execCommand(`node ${__dirname}/src/script.js --base=${Number(n)}`) )