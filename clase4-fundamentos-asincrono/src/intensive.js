let loopLimit = 1e9

module.exports = {
  simulateSync: () => {
    console.log('Comenzando bloqueo simulado...')
    for(let i = 0; i<= loopLimit ; i++){
      if (i === loopLimit)
        console.log('He llegado al final')
    }
    console.log('El bucle ha finalizado!')
  },
  simulateAsync: () => {
    console.log('Comenzando bloqueo simulado...')
    setImmediate( ()=> {
      for (let i = 0; i <= loopLimit; i++) {
        if (i === loopLimit)
          console.log('He llegado al final')
      }
    })
    console.log('El bucle ha finalizado!')
  }
}