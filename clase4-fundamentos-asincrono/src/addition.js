module.exports = {
  //VERSION SINCRONA
  syncSum: function (a,b) {
    console.log(a+b);
  },
  //VERSION ASINCRONA
  asyncSum: function(a,b) {
    setTimeout(()=> {
      console.log(a+b)
    },5000)
  }
}