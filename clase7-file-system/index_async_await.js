const fs = require("fs");
const ops = require('./src/fileops')

const readFile = (path) => {
  return new Promise((resolve, reject) => {
    fs.readFile(path, 'utf8', (err, list) => {
      if (err) reject(err);

      resolve(list.split('\n'));
    });
  });
}

const writeFile = (path, value) => {
  return new Promise((resolve, reject) => {
    fs.writeFile(path, value, (err, list) => {
      if (err) reject(err);
      resolve(true);
    });
  });
}

const main = async function() {
  try {
    //Number
    const fileReaded= await readFile(`${__dirname}/resources/number.txt`)
    const newNumbers = ops.incrementValues(fileReaded.map(n => Number(n)));
    const save = await writeFile(`${__dirname}/resources/number_async_await.txt`, newNumbers.join("\n"))

    //Names
    const fileReaded2= await readFile(`${__dirname}/resources/name.txt`)
    const names = ops.callNames(fileReaded2)
    const save2 = await writeFile(`${__dirname}/resources/names_async_await.txt`, names.join("\n"))

    console.log(save)
    console.log(save2)
  } catch (error) {
    console.log(error)
  }
}

main();