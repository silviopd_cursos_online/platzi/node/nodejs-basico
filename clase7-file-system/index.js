const fs = require('fs')
const ops = require('./src/fileops')

let data

fs.readFile(`${__dirname}/resources/number.txt`,'utf8',(err,data) => {
  if (err) throw err;
  // console.log(data)

  const numbers = data.split("\n").map(n => Number(n) )
  // console.log(numbers)

  // console.log(ops.incrementValues(numbers))
  data = ops.incrementValues(numbers)

  fs.writeFile(`${__dirname}/resources/number_new.txt`, data.join("\n") ,(err,result) => {
    if (err) throw err;
    // console.log(data)
  })
})

