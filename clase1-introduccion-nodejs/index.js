function a() {
  console.log('Estoy imprimiendo la llamada a la función A.')
};

function b() {
  console.log('Estoy imprimiendo la llamada a la función B.')
}

function c() {
  console.log('Estoy imprimiendo la llamada a la función C.')
}

d = () => {
  console.log('Estoy imprimiendo la llamada a la función D.')
}

a();
b();
c();
d();