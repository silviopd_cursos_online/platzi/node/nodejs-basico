const readline = require('readline')
const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout
})

const q = [
  "¿Cuál es tu primer nombre? \n",
  "¿Cuál es tu primer apellido? \n",
  "¿Cuál es tu edad? \n"
]

const AskQuestion = (rl, question) => {
  return new Promise( (res,rej) => {
    rl.question(question, ans => {
      res(ans)
    })
  })
}

const Ask = (questions) => {
  return new Promise ( async resolve => {
    let results = []
    for (let i=0 ; i< questions.length; i++){
      const result = await AskQuestion(rl,questions[i])
      results.push(result)
    }
    rl.close()
    resolve(results)
  })
}

Ask(q).then( q => {
  console.log(`Hola ${q[0]} ${q[1]}, tu edad es ${q[2]} años`)
})